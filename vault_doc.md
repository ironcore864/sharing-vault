# Vault Admin Policy

https://www.hashicorp.com/resources/policies-vault/

# Revoke Root Token After Initial Setup (AuthN, Policy, etc)

# OIDC Auth

With GitLab OIDC:

https://docs.gitlab.com/ee/integration/openid_connect_provider.html

```
vault write auth/oidc/config \
    oidc_discovery_url="https://gitlab.com" \
    oidc_client_id="9743b00d700438f9e5364120098c3b4b1aceb337c409650070993d9397c2324f" \
    oidc_client_secret="ce5f947507b3b07298b47beb4493f025eed3d46367b3566aef5026193e17d817" \
    default_role="default" \
    bound_issuer="localhost"

vault write auth/oidc/role/default -<<EOF
{
  "user_claim": "sub",  
  "bound_claims": {
    "groups": "seirobotics"
  },
  "allowed_redirect_uris": "http://localhost:8250/oidc/callback,https://vault.skywayplatform.com/ui/vault/auth/oidc/oidc/callback",
  "bound_audiences": "9743b00d700438f9e5364120098c3b4b1aceb337c409650070993d9397c2324f",
  "role_type": "oidc",
  "oidc_scopes": "openid",
  "policies":"default",
  "ttl": "1h"
}
EOF

vault write auth/oidc/role/admin -<<EOF
{
  "user_claim": "sub",  
  "bound_claims": {
    "groups": "seirobotics",
    "email": "guotiexin@gmail.com"
  },
  "allowed_redirect_uris": "http://localhost:8250/oidc/callback,https://vault.skywayplatform.com/ui/vault/auth/oidc/oidc/callback",
  "bound_audiences": "9743b00d700438f9e5364120098c3b4b1aceb337c409650070993d9397c2324f",
  "role_type": "oidc",
  "oidc_scopes": "openid",
  "policies":"admin",
  "ttl": "1h"
}
EOF
```

```
vault login -method=oidc
vault login -method=oidc role=admin
```

# AppRole Auth

https://www.vaultproject.io/docs/auth/approle

```
vault auth enable approle

vault write auth/approle/role/rolename \
    secret_id_ttl=72h \
    token_num_uses=1000 \
    token_ttl=72h \
    token_max_ttl=72h \
    secret_id_num_uses=4000 \
    policies=rolename

vault read auth/approle/role/rolename/role-id

vault write -f auth/approle/role/rolename/secret-id
```
