# Manage auth methods broadly across Vault
path "auth/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "aws/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

# Create, update, and delete auth methods
path "sys/auth/*" {
  capabilities = ["create", "update", "delete", "sudo"]
}

# List auth methods
path "sys/auth" {
  capabilities = ["read"]
}

# Read self policy
path "sys/policies/acl/admin" {
  capabilities = ["read", "list"]
}

# List policies
path "sys/policies/acl" {
  capabilities = ["list"]
}

# Create and manage secrets engines broadly across Vault.
path "sys/mounts/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Read health checks
path "sys/health" {
  capabilities = ["read", "sudo"]
}

# To perform Step 4
path "sys/capabilities" {
  capabilities = ["create", "update"]
}

# To perform Step 4
path "sys/capabilities-self" {
  capabilities = ["create", "update"]
}

# Allow access secrets "drone/"" (kv), for CI
path "drone/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

# Allow access secrets "kv/" (kv v2), default KV engine
path "kv/data/*" {
  capabilities = ["create", "read", "delete", "update"]
}

path "kv/metadata/*" {
  capabilities = ["list", "read"]
}

path "kv/delete/*" {
  capabilities = ["update"]
}
